import Vue from "vue";

class ThemeService {
  private darkMode = false;
  constructor() {
    if (this.isModeStored()) {
      this.darkMode = this.getStoreMode();
    }
  }

  setDarkMode(dark: boolean): void {
    this.darkMode = dark;
    this.setStoreMode(this.darkMode);
  }

  isDarkMode(): boolean {
    return this.darkMode;
  }

  forceCheckMode(vue: Vue) {
    this.setVuetifyMode(vue, this.darkMode);
  }

  toggleMode(vue: Vue): boolean {
    const toggle = !this.darkMode;
    this.setVuetifyMode(vue, toggle);
    this.setDarkMode(toggle);

    return this.darkMode;
  }

  setVuetifyMode(vue: Vue, dark: boolean): void {
    vue.$vuetify.theme.dark = dark;
  }

  getVuetifyMode(vue: Vue) {
    return vue.$vuetify.theme.dark;
  }

  setStoreMode(dark: boolean): void {
    localStorage.darkMode = dark;
  }

  getStoreMode(): boolean {
    return JSON.parse(localStorage.darkMode);
  }

  isModeStored(): boolean {
    if (localStorage.darkMode) {
      return true;
    }

    return false;
  }
}

export default new ThemeService();
